package com.example.fcmtokenpushnotification;

import android.content.Context;
import android.content.SharedPreferences;

public class SharingMyData {

    public static Context mcontext;
    public static final String SHAREDPREF = "sharing ";
    public static final String TEXT = "text";
    public static SharingMyData mInstance;

    public SharingMyData(Context mcontext) {
        this.mcontext = mcontext;
    }

    public static SharingMyData getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharingMyData(context);
        }
        return mInstance;
    }

    public void saveInSharedpref(String string) {

        SharedPreferences sharedPreferences = mcontext.getSharedPreferences(SHAREDPREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TEXT, string);
        editor.apply();

    }

    public String gettokenFromSF() {

        SharedPreferences sharedPreferences = mcontext.getSharedPreferences(SHAREDPREF, Context.MODE_PRIVATE);
        return sharedPreferences.getString(TEXT, "nhi mila ");


    }
}
