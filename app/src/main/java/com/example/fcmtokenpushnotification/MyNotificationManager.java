package com.example.fcmtokenpushnotification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;

import androidx.core.app.NotificationCompat;

public class MyNotificationManager {


    private Context mcontext;
    //         instance of this class
    private static MyNotificationManager mInstance;

    public MyNotificationManager(Context mcontext) {
        this.mcontext = mcontext;
    }

    //   this method is made to get the instance of this class in any other class yo use the methods of this class ...
    public static synchronized MyNotificationManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MyNotificationManager(context);
        }
        return mInstance;
    }
//  this is what the notification will be
//  two params are passed and will be invoked in onReceivedMessaged() in MyFirebaseInstanceIdService().class

    public void DisplayNotification(String title, String body) {

//        context and channel id is passed , so made new java class named constants
//        after oREO notification channel wont work if channel id is not passed
        NotificationCompat.Builder mbuilder =
                new NotificationCompat.Builder(mcontext, Constants.CHANNEL_ID)
                        .setLargeIcon(BitmapFactory.decodeResource(mcontext.getResources(),
                                R.mipmap.ic_launcher))
                        .setContentTitle(title)
                        .setContentText(body);


//       after gettin the msg user will click on it which will open a new new Activity , just to check
//       used MainActivty.class here ..
//
        Intent intent = new Intent(mcontext, MainActivity.class);

//         FLAG_UPDATE _CURRENT means that if notifictaion is already there on the screen
//        then it will just update the existing notification
//        it wont make a new notification if the same data having notification already exists ..

        PendingIntent pendingIntent =
                PendingIntent.getActivity(mcontext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mbuilder.setContentIntent(pendingIntent);

//      This class is a in built class not the one that i have created ...
        NotificationManager mNotificationManager =
                (NotificationManager) mcontext.getSystemService(Context.NOTIFICATION_SERVICE);


//      If notificationmanager is not empty then

        if (mNotificationManager != null) {
            mNotificationManager.notify(0, mbuilder.build());
        }
    }
}
