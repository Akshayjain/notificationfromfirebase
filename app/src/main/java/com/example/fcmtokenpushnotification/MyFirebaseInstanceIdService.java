package com.example.fcmtokenpushnotification;

import android.content.Context;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseInstanceIdService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseInstanceIdSer";
    static Context mcontext;
    public static MyFirebaseInstanceIdService myFirebaseInstanceIdServiceInstance;

    public MyFirebaseInstanceIdService() {
    }

    public MyFirebaseInstanceIdService(Context mcontext) {
        this.mcontext = mcontext;
    }

    public static MyFirebaseInstanceIdService getInstance(Context context) {
        if (myFirebaseInstanceIdServiceInstance == null) {
            myFirebaseInstanceIdServiceInstance = new MyFirebaseInstanceIdService(context);

        }
        return myFirebaseInstanceIdServiceInstance;
    }

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "fromMytoken Real: " + token);
//        here a token will be generated which will be used to send notification from firebase server
//        todo save this notification in sharedprefrences


    }

    public static void savetoken(String token) {
        SharingMyData.getInstance(mcontext).saveInSharedpref(token);

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

//        here i ll be receiving message from firebase thats why created
//        MynotificationManager instance here top display to the notification
        Log.d(TAG, "onMessageReceived: " + remoteMessage.getFrom());

        String title = remoteMessage.getNotification().getTitle();
        String body = remoteMessage.getNotification().getBody();

        MyNotificationManager.getInstance(getApplicationContext()).DisplayNotification(title, body);
    }


}
